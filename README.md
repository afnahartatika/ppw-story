[![pipeline status][pipeline-badge]][commits-gl]
[![coverage report][coverage-badge]][commits-gl]

[pipeline-badge]: https://gitlab.com/afnahartatika/ppw-story/badges/master/pipeline.svg
[commits-gl]: https://gitlab.com/afnahartatika/ppw-story/-/commits/master
[coverage-badge]: https://gitlab.com/afnahartatika/ppw-story/badges/master/coverage.svg
