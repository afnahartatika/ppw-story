from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, 'index.html')

def memories(request):
    return render(request, 'memories.html')