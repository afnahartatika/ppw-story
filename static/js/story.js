$(document).ready(function () {
    $('.accordion-toggle').on('click', function(event){
    	event.preventDefault();
    	// create accordion variables
    	var accordion = $(this);
    	var accordionContent = accordion.next('.card-body');
    	accordionContent.slideToggle(250); // duration
    	
    });
    
    // down button
    $('#down').click(function (e) {
        var self = $(this),
            item = self.parents('div.card'),
            swapWith = item.next();
        item.before(swapWith.detach());
  	});

    // up button
    $('#up').click(function (e) {
        var self = $(this),
            item = self.parents('div.card'),
            swapWith = item.prev();
        item.after(swapWith.detach());
    });
});