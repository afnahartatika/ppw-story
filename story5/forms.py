from django import forms
from .models import Schedule

class ScheduleForm(forms.Form):
    
    course = forms.CharField(
        label = 'Course', 
        max_length = 100, 
        required = True,
    )

    lecturer = forms.CharField(
        label = 'Lecturer', 
        max_length = 100, 
        required = True,
    )

    credit = forms.IntegerField(
        label = 'Credits', 
        required = True,
    )

    description = forms.CharField(
        label = 'Description', 
        max_length = 150, 
        required = True,
    )

    term = forms.CharField(
        label = 'Term', 
        max_length = 100, 
        required = True,
        widget = forms.TextInput(attrs ={'placeholder': 'Example: 2020/2021'}),
    )

    classroom = forms.CharField(
        label = 'Classroom', 
        max_length = 100, 
        required = True,
    )


