from django.db import models

# Create your models here.
class Schedule(models.Model):
    course = models.CharField(max_length=300)
    lecturer = models.CharField(max_length=300)
    credit = models.IntegerField()
    description = models.TextField(max_length=350)
    term = models.CharField(max_length=300)
    classroom = models.CharField(max_length=300)