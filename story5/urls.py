from django.urls import path
from . import views

app_name = 'story5'

urlpatterns = [
    path('form', views.schedule, name='schedule'),
    path('list', views.list_course, name='list_course'),
    path('details/<int:index>', views.details_course, name='details_course'),
]