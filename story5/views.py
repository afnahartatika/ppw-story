from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .models import Schedule
from .forms import ScheduleForm
from story5 import forms, models

# Create your views here.

def schedule(request):
    schedule = Schedule.objects.all().values()
    form = ScheduleForm(request.POST or None)
    context = {
        'schedule':schedule,
        'form':form,
    }
    if request.method == "POST":
        if form.is_valid():
            schedule.create(
                course = form.cleaned_data.get('course'),
                lecturer = form.cleaned_data.get('lecturer'),
                credit = form.cleaned_data.get('credit'),
                description = form.cleaned_data.get('description'),
                term = form.cleaned_data.get('term'),
                classroom = form.cleaned_data.get('classroom'),
            )
        return redirect('/story5/list')
           
    return render(request, 'schedule.html', context)

def list_course(request):
    schedule = Schedule.objects.all().values()
    context = {'schedule':schedule}
    if request.method == "POST":
        if 'id' in request.POST:
            Schedule.objects.get(id=request.POST['id']).delete()
            return redirect('/story5/list')
    list_course = Schedule.objects.all()
    return render(request, 'list_course.html', {'schedule':list_course})

def details_course(request, index):
    schedule = Schedule.objects.get(pk=index)
    context = {'schedule':schedule}
    return render(request, 'details_course.html', context)