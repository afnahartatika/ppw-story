from django import forms
from .models import Activity, Participant

class ActivityForm(forms.ModelForm):
    class Meta:
        model = Activity
        fields = '__all__'
        widgets = {
            'activity':forms.TextInput(attrs={
                'class':'form-control',
            }),
            'category':forms.TextInput(attrs={
                'class':'form-control',
                'placeholder':'Example: Arts, Sports',
            })
        }


class ParticipantForm(forms.ModelForm):
    class Meta:
        model = Participant
        fields = '__all__'
        widgets = {
            'name':forms.TextInput(attrs={
                'class':'form-control',
            })
        }
