from django.db import models

# Create your models here.
class Activity(models.Model):
    activity = models.CharField(max_length=100)
    category = models.CharField(max_length=100)
    def __str__(self):
        return self.activity
    
class Participant(models.Model):
    name = models.CharField(max_length=100)
    activity = models.ForeignKey(Activity, on_delete=models.CASCADE, null=True)
    def __str__(self):
        return self.name