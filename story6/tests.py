from django.test import TestCase, Client
from django.urls import reverse, resolve
from django.apps import apps

from .models import Activity, Participant
from .apps import Story6Config

# Create your tests here.
class Unit_Test(TestCase):
    def test_url_activity(self):
        response = Client().get("/story6/activity")
        self.assertEqual(response.status_code, 200)

    def test_templates_list_activity(self):
        response = Client().get("/story6/activity")
        self.assertTemplateUsed(response, 'list_activity.html')

    def test_url_add_activity(self):
        response = Client().get("/story6/activity/add")
        self.assertEqual(response.status_code, 200)

    def test_templates_add_activity(self):
        response = Client().get("/story6/activity/add")
        self.assertTemplateUsed(response, 'add_activity.html')

    def test_text_activity_at_list_activity(self):
        response = Client().get("/story6/activity")
        html_response = response.content.decode('utf8')
        self.assertIn("Activity", html_response)
        self.assertIn("Add and Join Activity", html_response)

    def test_text_Submit_Join_at_add_activity(self):
        response = Client().get("/story6/activity/add")
        html_response = response.content.decode('utf8')
        self.assertIn("Submit", html_response)
        self.assertIn("Join", html_response)
        self.assertIn("Back", html_response)
        self.assertIn("Activity", html_response)
        self.assertIn("Name", html_response)
        self.assertIn("Category", html_response)

class TestApp(TestCase):
	def test_apps(self):
		self.assertEqual(Story6Config.name, 'story6')
		self.assertEqual(apps.get_app_config('story6').name, 'story6')

        