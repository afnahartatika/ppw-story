from django.urls import path
from . import views

app_name = 'story6'

urlpatterns = [
    path('activity', views.list_activity, name='list_activity'),
    path('activity/add', views.add, name='add'),
    path('delete/<int:index>', views.delete_activity, name='delete_activity'),
    path('delete/participant/<int:index>', views.delete_participant, name='delete_participant'),
]