from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .models import Activity, Participant
from .forms import ActivityForm, ParticipantForm
from story6 import forms, models

# Create your views here.
def list_activity(request):
    activity = Activity.objects.all()
    participant = Participant.objects.all()
    form_activity = ActivityForm(request.POST)
    form_participant = ParticipantForm(request.POST)
    context = {
        'activity':activity,
        'participant':participant,
        'form_activity':form_activity,
        'form_participant':form_participant,
    }

    return render(request, 'list_activity.html', context)

def add(request):
    activity = Activity.objects.all()
    participant = Participant.objects.all()
    form_activity = ActivityForm(request.POST)
    form_participant = ParticipantForm(request.POST)
    context = {
        'activity':activity,
        'participant':participant,
        'form_activity':form_activity,
        'form_participant':form_participant,
    }

    if request.method == "POST":
        if form_activity.is_valid():
            form_activity.save()
            return redirect('/story6/activity')
        elif form_participant.is_valid():
            form_participant.save()
            return redirect('/story6/activity')
    return render(request, 'add_activity.html', context)


def delete_activity(request, index):
    activity = Activity.objects.get(id=index)
    activity.delete()
    return redirect('/story6/activity')


def delete_participant(request, index):
    participant = Participant.objects.get(id=index)
    participant.delete()
    return redirect('/story6/activity')