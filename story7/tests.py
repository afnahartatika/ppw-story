from django.test import TestCase, Client
from django.apps import apps

from .apps import Story7Config

# Create your tests here.
class Unit_Test(TestCase):
    def test_url_story7(self):
        response = Client().get("/story7/")
        self.assertEqual(response.status_code, 200)

    def test_templates_story7(self):
        response = Client().get("/story7/")
        self.assertTemplateUsed(response, 'story7.html')

    def test_text_story7(self):
        response = Client().get("/story7/")
        html_response = response.content.decode('utf8')
        self.assertIn("About Me", html_response)
        self.assertIn("Current Activity", html_response)
        self.assertIn("Experience", html_response)
        self.assertIn("Achievement", html_response)
        self.assertIn("Contact me", html_response)

class TestApp(TestCase):
	def test_apps(self):
		self.assertEqual(Story7Config.name, 'story7')
		self.assertEqual(apps.get_app_config('story7').name, 'story7')

