from django.test import TestCase, Client
from django.apps import apps

from .apps import Story8Config

# Create your tests here.
class Unit_Test(TestCase):
    def test_url_story8(self):
        response = Client().get("/story8/")
        self.assertEqual(response.status_code, 200)

    def test_templates_story8(self):
        response = Client().get("/story8/")
        self.assertTemplateUsed(response, 'books.html')

    def test_text_story7(self):
        response = Client().get("/story8/")
        html_response = response.content.decode('utf8')
        self.assertIn("Books", html_response)
        self.assertIn("Cover", html_response)
        self.assertIn("Title", html_response)
        self.assertIn("Author", html_response)
        self.assertIn("Publisher", html_response)
        self.assertIn("Published Date", html_response)
        self.assertIn("Contact me", html_response)

    def test_search_book_url(self):
        response = Client().get('/story8/data/?q=spongebob')
        self.assertEqual(response.status_code, 200)

class TestApp(TestCase):
	def test_apps(self):
		self.assertEqual(Story8Config.name, 'story8')
		self.assertEqual(apps.get_app_config('story8').name, 'story8')

