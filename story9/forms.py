from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User
from .models import UserModels

class SignUpForm(UserCreationForm):
    
    first_name = forms.CharField(label = "First name", widget = forms.TextInput(attrs = {
        'class' : 'form-control',
        'id' : 'signUp-firstName',
        'type' : 'text',
        'required' : True,
    }))

    last_name = forms.CharField(label = "Last name", widget = forms.TextInput(attrs = {
        'class' : 'form-control',
        'id' : 'signUp-lastName',
        'type' : 'text',
        'required' : True,
    }))

    username = forms.CharField(label = "Username", widget = forms.TextInput(attrs = {
        'class' : 'form-control',
        'id' : 'signUp-username',
        'type' : 'text',
        'required' : True,
    }))

    email = forms.EmailField(label = "Email", widget = forms.EmailInput(attrs = {
        'class' : 'form-control',
        'id' : 'signUp-email',
        'type' : 'email',
        'required' : True,
    }))

    password1 = forms.CharField(label = "Password", widget = forms.PasswordInput(attrs = {
        'class' : 'form-control',
        'id' : 'signUp-password1',
        'type' : 'password',
        'required' : True,
    }))

    password2 = forms.CharField(label = "Password confirmation", widget = forms.PasswordInput(attrs = {
        'class' : 'form-control',
        'id' : 'signUp-password2',
        'type' : 'password',
        'required' : True,
    }))

    class Meta:
        model = User
        fields = [
            'username',
            'first_name',
            'last_name',
            'email',
            'password1',
            'password2',
        ]


class LoginForm(AuthenticationForm):
    username = forms.CharField(label = "Username", widget = forms.TextInput(attrs = {
        'class' : 'form-control',
        'id' : 'signUp-username',
        'type' : 'text',
        'required' : True,
    }))

    password = forms.CharField(label = "Password", widget = forms.PasswordInput(attrs = {
        'class' : 'form-control',
        'id' : 'signUp-password',
        'type' : 'password',
        'required' : True,
    }))

    class Meta:
        model = User
        fields = [
            'username',
            'password'
        ]

