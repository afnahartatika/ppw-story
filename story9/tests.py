from django.test import TestCase, Client
from django.apps import apps

from .apps import Story9Config

# Create your tests here.
class Unit_Test(TestCase):
    def test_url_login(self):
        response = Client().get("/login/")
        self.assertEqual(response.status_code, 200)

    def test_templates_login(self):
        response = Client().get("/login/")
        self.assertTemplateUsed(response, 'login.html')

    def test_url_login_post(self):
        response = Client().post("/login/")
        self.assertEqual(response.status_code, 200)

    def test_templates_login_post(self):
        response = Client().post("/login/")
        self.assertTemplateUsed(response, 'login.html')

    def test_url_signup(self):
        response = Client().get("/signup/")
        self.assertEqual(response.status_code, 200)

    def test_url_signup_post(self):
        response = Client().post("/signup/")
        self.assertEqual(response.status_code, 200)

    # def test_templates_signup_post(self):
    #     response = Client().post("/signup/")
    #     self.assertTemplateUsed(response, 'login.html')

    # def test_templates_signup(self):
    #     response = Client().get("/signup/")
    #     self.assertTemplateUsed(response, 'signup.html')

    def test_url_logout(self):
        response = Client().get("/logout/")
        self.assertEqual(response.status_code, 302)

    # def test_templates_signup(self):
    #     response = Client().get("/logout/")
    #     self.assertTemplateUsed(response, 'login.html')

    def test_text_login(self):
        response = Client().get("/login/")
        html_response = response.content.decode('utf8')
        self.assertIn("Sign In", html_response)
        self.assertIn("Username", html_response)
        self.assertIn("Password", html_response)

    # def test_text_signup(self):
    #     response = Client().get("/signup/")
    #     html_response = response.content.decode('utf8')
        # self.assertIn("Sign Up", html_response)
        # self.assertIn("Username", html_response)
        # self.assertIn("Password", html_response)
        # self.assertIn("Password confirmation", html_response)
        # self.assertIn("First name", html_response)
        # self.assertIn("Last name", html_response)

class TestApp(TestCase):
	def test_apps(self):
		self.assertEqual(Story9Config.name, 'story9')
		self.assertEqual(apps.get_app_config('story9').name, 'story9')