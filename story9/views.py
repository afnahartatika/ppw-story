from django.shortcuts import render, redirect 
from .forms import SignUpForm, LoginForm
from .models import UserModels
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages

# Create your views here.

def logIn(request):
    if request.method == 'POST':
        form = LoginForm(data = request.POST)

        if form.is_valid():
            username_input = request.POST['username']
            password_input = request.POST['password']
            user = authenticate(request, username = username_input, password = password_input)
            
            if user is not None:
                login(request, user)
                return redirect('/login')
        
        else:
            messages.error(request, 'Invalid Login!')

    else:
        form = LoginForm()

    context = {
        'form' : form,
    }

    return render(request, 'login.html', context)

def signUp(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)

        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect('/login')

        else:
            for i in form.error_messages:
                messages.error(request, 'Invalid entry')

    else:
        form = SignUpForm()

    context = {
        'form' : form
    }

    return render(request, 'signup.html', context)

def logOut(request):
    logout(request)
    return redirect('/login')